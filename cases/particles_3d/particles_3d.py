import salome
salome.salome_init()

import GEOM

study_builder = salome.myStudy.NewBuilder()

from salome.geom import geomBuilder
geompy = geomBuilder.New(salome.myStudy)

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)

import math

W = 40.0
H = 50.0
D = 40.0

origin = geompy.MakeVertex(0, 0, 0)
origin_s = geompy.MakeVertex(0, D, 0)

y_axis = geompy.MakeEdge(origin, origin_s)

medium = geompy.MakeBox(-W, -D, 0, W, D, H)

particle_infos = []
for ln in open("../particles.list"):
    x,y,z,r = map(float, ln.split())
    particle_infos.append((geompy.MakeVertex(x,y,z), r))


propellant = geompy.MakePartition([medium],
                                  [geompy.MakeSpherePntR(c, r) for c,r in particle_infos],
                                  #+ [geompy.MakeSpherePntR(c, r/4) for c,r in particle_infos],
                                  [], [], geompy.ShapeType["SOLID"])
geompy.addToStudy(propellant, "propellant")


tmesh = smesh.Mesh(propellant, "propellant")

submesh = []
for i,(c,r) in enumerate(particle_infos):
    inner = geompy.GetBlockNearPoint(propellant, geompy.MakeVertexWithRef(c, 0, 0, 0))
    #outer = geompy.GetBlockNearPoint(propellant, geompy.MakeVertexWithRef(c, 0, 0, r/2))
    #inner_face = geompy.GetFaceNearPoint(propellant, geompy.MakeVertexWithRef(c, 0, 0, r/4))
    #outer_face = geompy.GetFaceNearPoint(propellant, geompy.MakeVertexWithRef(c, 0, 0, r))

    # tmesh.Triangle(geom=inner_face)
    # tmesh.Segment(inner_face).MaxSize(5e-6)
    # tmesh.Tetrahedron(geom=inner)

    # tmesh.Projection1D2D(geom=outer_face).SourceFace(inner_face, tmesh)

    # tmesh.Prism('RadialPrism_3D', geom=outer).NumberOfLayers(int(math.ceil(r/1e-5)))

    #tmesh.Segment(inner).MaxSize(1)
    algo = tmesh.Tetrahedron(geom=inner,algo=smeshBuilder.NETGEN_1D2D3D)
    algo.SetMinSize(1)
    algo.SetMaxSize(1)

binder = geompy.GetBlockNearPoint(propellant, geompy.MakeVertex(-W-1e-6, D/2, H/2))
algo = tmesh.Tetrahedron(geom=binder)
algo.SetMinSize(1)
algo.SetMaxSize(1)

tmesh.Segment().MaxSize(1)
tmesh.Triangle()
tmesh.Tetrahedron()

sides = [ geompy.GetFaceNearPoint(binder, geompy.MakeVertex(-W, D/2, H/2)),
          geompy.GetFaceNearPoint(binder, geompy.MakeVertex(W, D/2, H/2)),
          geompy.GetFaceNearPoint(binder, geompy.MakeVertex(W/2, -D, H/2)),
          geompy.GetFaceNearPoint(binder, geompy.MakeVertex(W/2, D, H/2)),
]
tmesh.GroupOnGeom(geompy.MakeCompound(sides), "sides")

top = geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(0, D/2, 0))
tmesh.GroupOnGeom(top, "top")
bottom = geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(0, D/2, H))
tmesh.GroupOnGeom(bottom, "bottom")

tmesh.Compute()
