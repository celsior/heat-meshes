import salome
salome.salome_init()

import GEOM

study_builder = salome.myStudy.NewBuilder()

from salome.geom import geomBuilder
geompy = geomBuilder.New(salome.myStudy)

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)


W = 40.0
H = 50.0
dy = 1.0

origin = geompy.MakeVertex(0, 0, 0)
origin_s = geompy.MakeVertex(0, dy, 0)

y_axis = geompy.MakeEdge(origin, origin_s)

medium = geompy.MakeBox(-W, 0, 0, W, dy, H)

particle_infos = []
for ln in open("../particles.list"):
    x,y,z,r = map(float, ln.split())
    particle_infos.append((geompy.MakeVertex(x,y,z), r))

propellant = geompy.MakePartition([medium],
                                  [geompy.MakeCylinder(c, y_axis, r, dy) for c,r in particle_infos],
                                  [], [], geompy.ShapeType["SOLID"])
geompy.addToStudy(propellant, "propellant")


tmesh = smesh.Mesh(propellant, "propellant")

submesh = []
for i,(c,r) in enumerate(particle_infos):
    p = geompy.GetBlockNearPoint(propellant, c)
    front = geompy.GetFaceNearPoint(p, geompy.MakeVertexWithRef(c, 0, 0, 0))
    edge = geompy.GetEdgeNearPoint(p, geompy.MakeVertexWithRef(c, 0, dy/2, r/2))
    tmesh.Segment(front).MaxSize(1)
    tmesh.Segment(edge).NumberOfSegments(1)
    algo = tmesh.Quadrangle(algo=smeshBuilder.RADIAL_QUAD,geom=front)
    submesh.append(algo.GetSubMesh())

p = geompy.GetBlockNearPoint(propellant, geompy.MakeVertex(-W/2,0,1e-6))

edge = geompy.GetEdgeNearPoint(propellant, geompy.MakeVertexWithRef(origin, -W, dy/2, 0))
tmesh.Segment(edge).NumberOfSegments(1)

front = geompy.GetFaceNearPoint(propellant, geompy.MakeVertexWithRef(origin, -W/2, 0, 1e-6))
tmesh.Segment(front).MaxSize(1)
#algo = tmesh.Triangle(algo=smeshBuilder.NETGEN,geom=front)
algo = tmesh.Triangle(geom=front)
submesh.append(algo.GetSubMesh())

tmesh.SetMeshOrder([submesh])

tmesh.Prism()
tmesh.Compute()

sides = [ geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(-W, dy/2, H/2)),
          geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(W, dy/2, H/2)),
]
tmesh.GroupOnGeom(geompy.MakeCompound(sides), "sides")


top = geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(0, dy/2, 0))
tmesh.GroupOnGeom(top, "top")
bottom = geompy.GetFaceNearPoint(propellant, geompy.MakeVertex(0, dy/2, H))
tmesh.GroupOnGeom(bottom, "bottom")
